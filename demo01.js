// js basic
// run command: node + filename

// require = new   // var: to declare variable
var assert = require("assert");

// whether the statement is true to decide if there is error;
assert.ok(true);
assert.equal(1,1);


// false values: false, undefined, 0, null, '', NaN
assert.ok(!false);
assert.ok(!undefined);
assert.ok(!0);
assert.ok(!null);
assert.ok(!'');
assert.ok(!NaN);
assert.ok(!(NaN==NaN));
assert.ok(isNaN(NaN));


// local variable and Global variable
// nodejs: Global object is "global"
// browser: "window"
// console.log(global);

function localTest(){
  var a = 1;  // local variable
  b = 2;      // global variable
  console.log(a);   // println()
}

localTest();

var globalTest = function(){
  return b;
};

// same thing
console.log("global b: " + global.b);  // better
console.log("global b: " + b);
console.log("global b: " + globalTest());

// global definition
// in nodejs: keyword 'gloabl'; in browser: keyword 'window'


// self invoke function: run without invoking from other place
(function () {
  console.log("Hello from self invoke function")
})();


// about function
function avg(){
  // arguments is a keyword: function parameter  -  array-like obejct
  console.log(arguments);
  var result = 0;
  for(var i = 0; i < arguments.length; i++){
    result += arguments[i];
  }
  return result / arguments.length;
}

console.log(avg(2,3,4,5,6,7));


// hoisting features: first use, then define, but is undefined
(function(){
  hello();   // function: could be invoked

  // hello1();  // not allowed

  console.log(i);  // variable: undefined
  console.log(j);

  // var's scope is in current function
  for(var i = 0; i < 5; i++){

  }

  console.log(i);
  var j;

  function hello(){
    console.log("hello");
  }

  var hello1 = function(){
    console.log("hello1");
  }

})();



// == and ===
var s1 = "abc"; // String literal
var s2 = new String("abc");  // Object
assert.ok(s1 == s2);   // only check value
console.log(typeof s1);
console.log(typeof s2);
assert.ok(!(s1 === s2)); // check value and type

var x1 = {name: 'Bob'};  // hashmap: key-value pair
var x2 = {name: 'Bob'};
assert.ok(!(x1 == x2));
assert.ok(!(x1 === x2));
console.log(x1.name);
console.log(x1["name"]);
var x = {"first name": "Bob"};
console.log(x["first name"]);
