// js data types

var assert = require("assert");

// declare a var without defining is "undefined"

// DataTypes: boolean, string, number, undefined, null, Symbol
console.log(typeof "abc" === "string");  // "string" === "string"
var myBoolean = true;
console.log(typeof myBoolean, typeof typeof myBoolean);  // typeof "boolean"
console.log(typeof null);   // "object"
console.log(typeof undefined);  // "undefined"
console.log(typeof []);     // "object"
console.log(typeof function(){});   // "function"


var test = function(){
  console.log("test");
};

function testWrap(x){
  if(typeof x === "function"){
    x();
  }
}

testWrap(test);


// myobj is a empty object
var myobj = {};
myobj.name = "Bob";
console.log(myobj);
var myobj1 = new Object();

var x = Object(100);
assert.equal(x,100);  // ==
assert.ok(!(x===100)); // x is object, 100 is number



var y = {name: "bob"};
var z = Object(y);
console.log(y==z);
console.log(y===z);

function isObject(obj){
  console.log(Object(obj));
  return obj === Object(obj);
}

console.log(isObject(123), typeof 123);
