// js Object

var assert = require('assert')

var alice = {
  name: 'Alice',
  age: 20,
  hello: function(){
    return "Hello, " + this.name;
  }
};

var keys = Object.keys(alice);
console.log(keys);
console.log(typeof keys);

keys.forEach(function(key, index){
  console.log(key);
  if(typeof alice[key] === "function"){
    console.log(alice[key]());
  }
  else{
    console.log(alice[key]);
  }
});



var props = Object.getOwnPropertyNames(alice);
console.log(props);

var arr = [1, "a", 3, {"name": "Bob"}, 5];

// Object.keys: return all enumerable own properties
console.log("Keys ", Object.keys(arr));

console.log("getOwnPropertyNames", Object.getOwnPropertyNames(arr));




var bob = {
  _name: "Bob",  // member  key-val pair

  // get, set method
  get name(){
    return this._name;
  },
  set name(str){
    this._name = str;
  }
}

console.log(bob._name);
bob.name = "Steve" ;     // set name() function: set _name to Steve
console.log(bob.name);   // get name() function
console.log(bob._name);  // get name by field
