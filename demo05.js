// JS: this
var assert = require("assert");

// constructor
function Vehicle(){
  this.price = 1000;  // global.price = 1000; (because Vehicle is global)
  // this make price a member of Vehicle
  age = 10; // age is global
  var name = 222;  // local
}

// this 会随着环境改变而改变

// instantiate an object, creating a local scope
var v = new Vehicle();
// console.log() run in the scope of global
console.log(v);
console.log("after new ", global.name);
console.log("after new ", v.name);

// call a function
var v2 = Vehicle();
console.log(v2);
console.log("after function invoke", global.price);



// safe constructor: only create instance once
function Person(name){
  // in global, if Person havan't been created, create it.
  if(!(this instanceof Person)){
    return new Person(name);
  }
  // otherwise, just change the name
  this.name = name;
}

console.log(Person('Bob'));


var bob = {
  name: "Bob",
  hello: function(a, b){
    return "Hello " + this.name;
  }
}

console.log(bob.hello());
var myHello = bob.hello;
console.log(myHello(1,2));
