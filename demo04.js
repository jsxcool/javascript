// js object property attributes and object status

// A property has 3 attributes: enumerable, writable, configurable
// (private/public)
/*
configurable:
true if and only if the type of this property descriptor may be changed and if
the property may be deleted from the corresponding object.

enumerable:
true if and only if this property shows up during enumeration of the properties
on the corresponding object.

writable
true if and only if the value associated with the property may be changed with
an assignment operator.
*/


// 3 attributes are all true (default)
var alice = {
  "name": "Alice"
};
alice.age = 30;
console.log(Object.keys(alice));
alice.age = 40;
console.log(alice.age);

// 3 attributes are all false (default)
Object.defineProperty(alice, "salary", {value: 1000})
console.log(Object.keys(alice));
alice.salary = 2000;
console.log(alice.salary);


//  Object.defineProperty() defines a new property directly on an object,
// or modifies an existing property on an object, and returns the object.
Object.defineProperty(alice,   // object
  "address", {                // property
  value: "princeton",         // descriptor
  enumerable: true,    // descriptor: means can add key
  configurable: true    // descriptor: means can delete key
})
console.log(Object.keys(alice));

delete alice.address;
delete alice.age;
console.log(Object.keys(alice));




Object.defineProperty(alice, "color", {
  configurable: true
});

alice.color = 100;
console.log(alice.color);

Object.defineProperty(alice, "color", {
  writable: true   // means can access the value of the key
});
alice.color = 100;
console.log(alice.color);




// Three status of an object: extensible, sealed, frozen
// (fianl/)
// default: extensible - true, sealed - false, frozen: false
var obj = { x: "abc" };
console.log("extensible ? ", Object.isExtensible(obj));
Object.preventExtensions(obj);
console.log("extensible ? ", Object.isExtensible(obj));

obj.y = 100;    // can't add
console.log(obj.y);



console.log("sealed ? ", Object.isSealed(obj));
Object.seal(obj);
console.log("sealed ? ", Object.isSealed(obj));
delete obj.x;    // can't add or delete
console.log(obj);

// frozen means we can't update its values (access key)
console.log("frozen ? ", Object.isFrozen(obj));
Object.freeze(obj);
console.log("frozen ? ", Object.isFrozen(obj));
obj.x = "def" ;
delete obj.x;
console.log(obj);

// Note: all primitive types are Frozen
var s = "abc";
console.log(typeof s);
console.log("frozen ? ", Object.isFrozen(s));
// ???? why changable ???
s = "lll"
console.log(s);
